package com.example.nychighschool.compose.Screens

import androidx.navigation.NamedNavArgument
import androidx.navigation.navArgument
import com.example.nychighschool.Navigation.SchoolArgType

sealed class Screen(val route: String, val navArguments: List<NamedNavArgument> = emptyList()) {
    data object HighSchoolList : Screen(route = "highSchoolList")

    data object HighSchoolDetails : Screen(
        route = "SchoolDetails/{schoolDbn}",
        navArguments = listOf(navArgument("schoolDbn") { type =  SchoolArgType() }) //It specifies the type of data that can be passed as the value of the "schoolDbn" argument.
    ) {
        fun createRoute(highschool: String) = "SchoolDetails/$highschool"
    }
}
