package com.example.nychighschool.ViewModel

// HighSchoolsViewModel.kt

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nychighschool.data.HighSchool
import com.example.nychighschool.Repository.HighSchoolsRepository
import com.example.nychighschool.data.SATscore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HighSchoolsViewModel : ViewModel() {

    private val repository = HighSchoolsRepository()
    private val _highSchools = MutableLiveData<List<HighSchool>>()
    val highSchools: LiveData<List<HighSchool>> = _highSchools

    private val _highschoolsatscore = MutableLiveData<List<SATscore>>()
    val highSchoolsatscore: LiveData<List<SATscore>> = _highschoolsatscore

    fun fetchHighSchools() {
        viewModelScope.launch {
            val schools = withContext(Dispatchers.IO) {
                repository.getHighSchools()
            }
            _highSchools.value = schools
        }
    }

    fun fetchHighSchoolSatScore() {
        viewModelScope.launch {
            try {
                val details = repository.fetchHighSchoolSatScore()
                _highschoolsatscore.value = details
            } catch (e: Exception) {
                // Handle error
            }
        }
    }
}
